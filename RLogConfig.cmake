cmake_minimum_required ( VERSION 3.19 )

set(RLOG_ROOT_DIR "${CMAKE_CURRENT_LIST_DIR}")

add_library(RLog INTERFACE IMPORTED GLOBAL)
set_target_properties(RLog PROPERTIES 
    INTERFACE_INCLUDE_DIRECTORIES ${RLOG_ROOT_DIR}
    LINKER_LANGUAGE CXX
)


target_include_directories(RLog INTERFACE
	"${RLOG_ROOT_DIR}"
)

